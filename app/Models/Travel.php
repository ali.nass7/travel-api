<?php

// App\Models\Travel::create(['name'=>'Some thing','description'=>'aaa','number_of_days'=>5]);
// App\Models\Tour::create(['travel_id'=>996f6a0a-a7fd-40f9-bb4b-005da6984cfa,'name'=>'sss','starting_date'=>now(),'ending_date'=>now()->addDays(2),'price'=>100]);
namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Concerns\HasUuids;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Travel extends Model
{
    use HasFactory, Sluggable, HasUuids;
    protected $table= 'travels';
    protected $fillable=[
        'is_public',
        'slug',
        'name',
        'description',
        'number_of_days',
    ];

    public function sluggable():array
    {
        return [
            'slug'=>['source'=>'name']
        ];
    }
    
    public function tours():HasMany
    {
        return $this->hasMany(Tour::class);
    }
    //laravel10
    public function numberOfNights():Attribute
    {
        return Attribute::make(get:fn($value,$attributes)=>$attributes['number_of_days'] - 1);
    }
    
    //laravel <10
    // public function getNumberOfNightsAttribute(){
    //     return $this->number_of_days-1;
    // }
    
    //in api بتغنيك عن route'travels/{travel:slug}/tours' بتصير  'travels/{travel}/tours'
    // public function getRouteKeyName()
    // {
    //     return 'slug';
    // }


}
