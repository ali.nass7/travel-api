<?php

// App\Models\Tour::create(['travel_id'=>'996f6a0a-a7fd-40f9-bb4b-005da6984cfa','name'=>'Tour','starting_date'=>'2023-06-14','ending_date'=>'2023-06-19','price'=>119.99]);

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Http\Requests\ToursListRequest;
use App\Http\Resources\TourResource;
use App\Models\Travel;
use App\Models\Tour;
use Illuminate\Http\Request;

class TourController extends Controller
{
    public function index(Travel $travel,ToursListRequest $request)
    {
 
        $tours= $travel->tours()
        ->when($request->priceFrom,function($query)use ($request){
            $query->where('price','>=',$request->priceFrom*100);
        })
        ->when($request->priceTo,function($query)use ($request){
            $query->where('price','<=',$request->priceTo*100);
        })
        ->when($request->dateFrom,function($query)use ($request){
            $query->where('starting_date','>=',$request->dateFrom);
        })
        ->when($request->dateTo,function($query)use ($request){
            $query->where('starting_date','<=',$request->dateTo);
        })
        ->when($request->sortBy && $request->sortOrder,function($query)use ($request){
            // if(!in_array($request->sortOrder,['asc','desc']))return;
            $query->orderBy($request->sortBy,$request->sortOrder);
        })
        ->orderBy('starting_date')
        ->paginate();
        return TourResource::collection($tours); 


    }
}
