<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Http\Resources\TravelResource;
use App\Models\Travel;
use Illuminate\Http\Request;

class TravelController extends Controller
{
    function index(){
        
        // $travels=Travel::where('is_public',true)->get();
        $travels=Travel::where('is_public',true)->paginate();
        return TravelResource::collection($travels);
    }
}
