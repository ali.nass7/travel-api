<?php

namespace App\Console\Commands;

use App\Models\Role;
use App\Models\User;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class CreateUserCommand extends Command
{
    
    protected $signature = 'users:create';
 
    protected $description = 'Creates a new user';

    public function handle()
    {
        $user['name']=$this->ask('Name Of the new user');
        $user['email']=$this->ask('Email Of the new user');
        $user['password']=Hash::make($this->secret('Password Of the new user'));
        $roleName=$this->choice('Role Of the new user',['Admin', 'editor'], 1);
        $role=Role::where('name',$roleName)->first();

        if(! $role)
        {
            $this->error('Role not found');
            return -1;
        }
        //@liFatehs$123
        DB::transaction(function() use ($user,$role){
            $newUser= User::create($user);
            $newUser->roles()->attach($role->id);
        });

        $this->info('User '.$user['email'],' created successfuly');

        return 0;
    }
}
